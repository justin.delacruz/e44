import { Col, Row } from "react-bootstrap";

import { HomePage } from "../components/HomePage"
import { useShoppingCart } from "../context/ShoppingCartContext";

export function Home(){
    const {items} = useShoppingCart()
    return (
        <>
        <h1>Home</h1>
        <h6>Bili na kayo!!</h6>
        <Row md={2} xs={1} lg={3} className="g-3">
            {items.map(item =>(
                <Col key={item.id}><HomePage {...item} /></Col>
            ))}
            
        </Row>
    </>
    )
}