import { Card } from "react-bootstrap";

type StoreItemProps ={
    image: string; 
}

export function HomePage ({image}: StoreItemProps){
    return (
    <Card className="h-200">
        <Card.Img variant="top" src={image} height="350px" style={
            {objectFit: "cover" }} />
        
    </Card>

)}